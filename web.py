from flask import Flask

web = Flask(__name__)

@web.route('/')
def helloIndex():
    return 'Hello World from Python Flask!'

web.run(host='0.0.0.0')